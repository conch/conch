%%%
%%%  conch -- Animate, simulate, and analyse concurrent programs.
%%%  Copyright (C) 2012 Marcel Kyas
%%%
%%%  This program is free software: you can redistribute it and/or modify
%%%  it under the terms of the GNU General Public License as published by
%%%  the Free Software Foundation, either version 3 of the License, or
%%%  (at your option) any later version.
%%%
%%%  This program is distributed in the hope that it will be useful,
%%%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%%%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%%  GNU General Public License for more details.
%%%
%%%  You should have received a copy of the GNU General Public License
%%%  along with this program.  If not, see <http://www.gnu.org/licenses/>.

\begin{code}
{-# OPTIONS_GHC -XDeriveDataTypeable -fno-cse #-}

import System.Console.CmdArgs.Implicit
import Conch.AST
import Conch.Parser

data Arguments = Arguments { inputs :: [String] }
  deriving (Show, Data, Typeable)

argSpec :: Arguments
argSpec = Arguments { inputs = def &= args &= typFile }
          &= summary "Conch v0.0, (c) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de> and others."
	  &= details ["Conch helps in understanding concurrent programs."]
	  &= program "conch"

-- | 'main' runs the main program
main :: IO ()
main = do arguments <- cmdArgs argSpec
          tree <- parseFromFile (head (inputs arguments))
	  print tree
\end{code}
