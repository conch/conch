%%%
%%%  conch -- Animate, simulate, and analyse concurrent programs.
%%%  Copyright (C) 2012 Marcel Kyas
%%%
%%%  This program is free software: you can redistribute it and/or modify
%%%  it under the terms of the GNU General Public License as published by
%%%  the Free Software Foundation, either version 3 of the License, or
%%%  (at your option) any later version.
%%%
%%%  This program is distributed in the hope that it will be useful,
%%%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%%%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%%  GNU General Public License for more details.
%%
%%%  You should have received a copy of the GNU General Public License
%%%  along with this program.  If not, see <http://www.gnu.org/licenses/>.

\section{Abstract Syntax}
\label{sec:conch:ast}

The module \lstinline|Conch.AST| specifies the abstract syntax of
\emph{Conch}'s input language. In addition, it defines a pretty
printer that exports the abstract syntax tree (AST) in the source
language itself. Iterator mechanisms are specified which simplify many
common tasks on abstract syntax trees. It provides a simple
substitution mechanism and various auxiliary functions for
manipulating AST. Finally, specifications of source code annotations
are given.

\begin{code}
{-# OPTIONS_GHC -W #-}
module Conch.AST where

import Text.PrettyPrint.HughesPJ as Pretty
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Foldable
\end{code}


\subsection{Annotations}
\label{sec:conch:ast:annotations}

Elements of the source code may carry annotations, e.g. file and line
number information to write out proper error messages or type
information.

\begin{code}
class Locatable note where
  getFile :: note -> String
  getLine :: note -> Int
  getColumn :: note -> Int
\end{code}

\begin{code}
class Typed note where
  getType :: note n -> Type n
  setType :: note n -> Type n -> note n
\end{code}

The default implementation of an annotation

\begin{code}
data Annotation n = Annotation String Int Int (Type n)

instance Locatable (Annotation n) where
  getFile (Annotation file _ _ _) = file
  getLine (Annotation _ line _ _) = line
  getColumn (Annotation _ _ column _) = column

instance Typed Annotation where
  getType (Annotation _ _ _ typ) = typ
  setType (Annotation file line column _) typ = Annotation file line column typ
\end{code}


\subsection{Types}
\label{sec:conch:ast:types}

The data type \lstinline|Type n| describes the type of an object.

\begin{code}
data Type n =
    Simple n String
  | Enumeration n [String]
  | Array n (Type n) [Expression n]
  | PredicateType n (Parameter n) (Expression n)
  deriving (Eq)

instance Show (Type n) where
  show typ = Pretty.render $ prettyType typ
\end{code}


\subsubsection{Functors for types}
\label{sec:conch:ast:types:functor}

The data type \lstinline|Type| implements the type class
\lstinline|Functor|, allowing to map a function over the annotation of
a type.

\begin{code}
instance Functor Type where
  fmap f (Simple n name) = Simple (f n) name
  fmap f (Enumeration n enumerators) = Enumeration (f n) enumerators
  fmap f (Array n o expr) = Array (f n) (fmap f o) (fmap (fmap f) expr)
  fmap f (PredicateType n parameter predicate) = PredicateType (f n) (fmap f parameter) (fmap f predicate)
\end{code}

As an example of an application of the functor, we demonstrate the
erase function, that replaces all annotations with the unit value.

\begin{code}
eraseAnnotation :: Type n -> Type ()
eraseAnnotation = fmap (\_ -> ())
\end{code}

\begin{code}
instance Foldable Type where
  foldr f a (Simple n _) = f n a
  foldr f a (Enumeration n _) = f n a
  foldr f a (Array n typ _) =
    f n (Data.Foldable.foldr f a typ)
  foldr f a (PredicateType n _ _) = f n a

prettyType :: Type n -> Pretty.Doc
prettyType (Simple _ name) = Pretty.text name
prettyType (Array _ typ range) =
  prettyType typ <> Pretty.brackets (prettyExprList range)
prettyType (Enumeration _ enumerators) =
  Pretty.braces $ Pretty.hsep (Pretty.punctuate Pretty.comma (map Pretty.text enumerators))
prettyType (PredicateType _ parameter predicate) =
  Pretty.braces $ prettyParameter parameter <+> Pretty.char '|' <+>
                    prettyExpr 0 predicate
\end{code}


\subsection{Expressions}
\label{sec:conch:ast:expressions}

The data type \lstinline|Expression n| specifies the expression
language algebraically. You should consult the function
\lstinline|show| below on how terms of type \lstinline|Expression|
might look in our language.

The type \lstinline|Expression n| is parametric in the type of
possible annotations. One example is \lstinline|Annotation| of
Sect.~\ref{sec:conch:ast:annotations}.

The abstract syntax is defined algebraically, with the following
intended meaning.

\begin{itemize}
\item \lstinline|LiteralTrue n| and
  \lstinline|LiteralFalse n| represent the Boolean literals.
\item \lstinline|LiteralInteger n value| represents a literal integer
  with value \lstinline|value|.
\item \lstinline|Identifier n| represents an identifier. This is usually
  a variable name. It can also refer to the name of constants, enumerators,
  or types, depending on context. 
\item \lstinline|Subscript| represents an \emph{array subscription
    expression}. The first argument to the constructor of type
  \texttt{Expression} indicates the array to access. The second
  argument, also of type \texttt{Expression} indicates the position to
  access.
\item \lstinline|Select| represents the selection of a field in an object or a
  structure. The first argument is a \lstinline|Expression| that represents
  the object to select from, and the second argument, a \texttt{String}
   is the name of the field.
\item \lstinline|Application| represents a function application
  term. The first argument, a \texttt{String}, is the name of the
  function, e.g.\ \texttt{"and"}, \texttt{"not"}, \texttt{"+"}.  The
  second argument is a list of \texttt{Expression}s representing the
  arguments to the function application.
\item \lstinline|Exists| and \lstinline|Forall| represent
  existentially and universally quantifified formulas. The first argument
  of type \lstinline|Parameter n| lists the name and types of variables
  that are quantified over in the second argument, a \lstinline|Expression|.
\item \lstinline|Comprehension| represents a list, set or similar,
  specified by comprehension.
\item \lstinline|EnumerationExpression| represents a list of values.
\item \lstinline|RangeExpression| represents a range of values.
\item \lstinline|SpawnExpression| creates a new process and starts it.
  The value of this expression is an identifier that allows to refer
  to the process.
\item \lstinline|Otherwise| is an expression used in conditional
  statements and loops to represent all uncovered cases.
\end{itemize}

\begin{code}
data Expression n = 
    LiteralTrue n
  | LiteralFalse n
  | LiteralInteger n Integer
  | Constant n String
  | Identifier n String
  | Subscript n (Expression n) (Expression n)
  | Select n (Expression n) String
  | Application n String [(Expression n)]
  | Exists n [Parameter n] (Expression n)
  | Forall n [Parameter n] (Expression n)
  | Comprehension n (Parameter n) (Expression n)
  | EnumerationExpression n [Expression n]
  | RangeExpression n (Expression n) (Expression n)
  | SpawnExpression n String [Expression n]
  | ConditionalExpression n (Expression n) (Expression n) (Expression n)
  | Otherwise n
  deriving (Eq)
\end{code}

The function below gives a textual representation of a term.

\begin{code}
instance Show (Expression n) where
  show expr = Pretty.render (prettyExpr 0 expr)
\end{code}

\begin{code}
instance Functor Expression where
  fmap f (LiteralTrue n) = LiteralTrue (f n)
  fmap f (LiteralFalse n) = LiteralFalse (f n)
  fmap f (LiteralInteger n value) = LiteralInteger (f n) value
  fmap f (Constant n value) = Constant (f n) value
  fmap f (Identifier n name) = Identifier (f n) name
  fmap f (Subscript n wher what) = Subscript (f n) (fmap f wher) (fmap f what)
  fmap f (Select n wher what) = Select (f n) (fmap f wher) what
  fmap f (Application n func args) = Application (f n) func (fmap (fmap f) args)
  fmap f (Exists n params expr) =
    Exists (f n) (fmap (fmap f) params) (fmap f expr)
  fmap f (Forall n params expr) =
    Forall (f n) (fmap (fmap f) params) (fmap f expr)
  fmap f (Comprehension n param expr) =
    Comprehension (f n) (fmap f param) (fmap f expr)
  fmap f (EnumerationExpression n enumerators) =
    EnumerationExpression (f n) (fmap (fmap f) enumerators)
  fmap f (RangeExpression n min max) =
    RangeExpression (f n) (fmap f min) (fmap f max)
  fmap f (SpawnExpression n process arguments) =
    SpawnExpression (f n) process (fmap (fmap f) arguments)
  fmap f (Otherwise n) = Otherwise (f n)
\end{code}



\subsubsection{Free variables in an expressions}
\label{sec:conch:ast:expression:free-vars}

Compute the set of free identifiers in a given expression. The result
includes both the free variables and all symbolic constants. This
funcion is used in the substitution function below
(see~\ref{sec:conch:ast:expression:substitution}).

\begin{code}
freeIdentifiers :: Expression n -> Set String
freeIdentifiers (Identifier _ name) = Set.singleton name
freeIdentifiers (Subscript _ wher what) =
  freeIdentifiers what `Set.union` freeIdentifiers wher
freeIdentifiers (Select _ wher _) = freeIdentifiers wher
freeIdentifiers (Application _ _ arguments) =
  Set.unions (map freeIdentifiers arguments)
freeIdentifiers (Exists _ binders body) =
  freeIdentifiers body `Set.difference` parameterNames binders
freeIdentifiers (Forall _ binders body) =
  freeIdentifiers body `Set.difference` parameterNames binders
freeIdentifiers (Comprehension _ binder body) =
  Set.delete (parameterName binder) (freeIdentifiers body)
freeIdentifiers (EnumerationExpression _ enumerators) =
  Set.unions (map freeIdentifiers enumerators)
freeIdentifiers (RangeExpression _ min max) =
  freeIdentifiers min `Set.union` freeIdentifiers max
freeIdentifiers (SpawnExpression _ _ arguments) =
  Set.unions (map freeIdentifiers arguments)
freeIdentifiers _ = Set.empty
\end{code}



\subsubsection{Substitution in expressions}
\label{sec:conch:ast:expression:substitution}

A substitution is given as a map associating to a string, the name of
the variable to be substituted, to a expression, i.e.\ the expression
to substitute the variable by.

\begin{code}
type Substitution n = Map String (Expression n)
\end{code}

The next function applies a substitution to an expression.

\begin{code}
substitute :: Substitution n -> Expression n -> Expression n
substitute _ (LiteralTrue n) = LiteralTrue n
substitute _ (LiteralFalse n) = LiteralFalse n
substitute _ (LiteralInteger n val) = LiteralInteger n val
substitute _ (Constant n val) = Constant n val
substitute subst (Identifier n name) | Map.member name subst =
  fmap (\_ -> n) (subst Map.! name)
substitute _ (Identifier n name) = Identifier n name
substitute subst (Subscript note wher what) =
  Subscript note (substitute subst wher) (substitute subst what)
substitute subst (Select note wher what) =
  Select note (substitute subst wher) what
substitute subst (Application note function arguments) =
  Application note function (map (substitute subst) arguments)
substitute subst (Exists note binders body) =
  Exists note binders (substitute subst body)
substitute subst (Forall note binders body) =
  Forall note binders (substitute subst body)
substitute subst (Comprehension note binders body) =
  Comprehension note binders (substitute subst body)
substitute subst (EnumerationExpression note enumerators) =
  EnumerationExpression note (map (substitute subst) enumerators)
substitute subst (RangeExpression note min max) =
  RangeExpression note (substitute subst min) (substitute subst max)
substitute subst (SpawnExpression note process arguments) =
  SpawnExpression note process (map (substitute subst) arguments)
substitute _ (Otherwise n) = Otherwise n
\end{code}



\subsubsection{Locally nameless representation}
\label{sec:conch:ast:expression:nameless}

In order to simplify the logical manipulation of expressions,
expressions may be converted into a locally nameless representation.

A test whether the variable name refers to a bound variable. We let
bound variables start with a \lstinline|$| and is followed by a
number.

\begin{code}
isBoundIdentifier :: Expression n -> Bool
isBoundIdentifier (Identifier _ name) = (head name) == '$'
isBoundIdentifier _ = False
\end{code}

Step the number of an expression.

\begin{code}
incNumber :: Expression n -> Expression n
incNumber (Identifier note name) | (head name) == '$' =
  Identifier note $ '$' : (show $ value + 1)
  where value = read $ tail name
incNumber expr = expr
\end{code}

\begin{code}
decNumber :: Expression n -> Expression n
decNumber (Identifier note name) | (head name) == '$' =
  Identifier note $ '$' : (show $ value - 1)
  where value = read $ tail name
decNumber expr = expr
\end{code}

The next function takes a expression and converts it to a locally
nameless representation.

\begin{code}
toLocallyNameless :: Map String Int -> Int -> Expression n -> Expression n
toLocallyNameless _ _ expr = expr
\end{code}



\subsubsection{Pretty printing expressions}
\label{sec:conch:ast:expression:pretty}

Build a document from an expression that can be rendered to different
formats. This uses the pretty library to abstract from proper line breaks
and indentiation when used in statements.

The first parameter refers to the associativity level of the binary
operator.

\begin{code}
prettyExpr :: Int -> Expression a -> Pretty.Doc
prettyExpr _ (LiteralTrue _) =
  Pretty.text "true"
prettyExpr _ (LiteralFalse _) =
  Pretty.text "false"
prettyExpr _ (LiteralInteger _ num) =
  Pretty.integer num
prettyExpr _ (Constant _ val) =
  Pretty.text val
prettyExpr _ (Identifier _ var) =
  Pretty.text var
prettyExpr _ (Subscript _ arr idx) =
  (prettyExpr 0 arr) <> (Pretty.brackets (prettyExpr 0 idx))
prettyExpr _ (Select _ expr label) =
  (prettyExpr 0 expr) <> (Pretty.char '.') <> (Pretty.text label)
prettyExpr prec (Application _ op [arg]) | Map.member op unaryOps =
  if operPrec < prec then Pretty.parens doc else doc
  where doc = Pretty.text op <+> prettyExpr operPrec arg
        operPrec = unaryOps Map.! op
prettyExpr prec (Application _ op [x, y]) | Map.member op binaryInfixOps =
  if operPrec < prec then Pretty.parens doc else doc
  where doc = prettyExpr operPrec x <+> Pretty.text op <+> prettyExpr operPrec y
        operPrec = binaryInfixOps Map.! op
prettyExpr _ (Application _ "If" [condition, ifTrue, ifFalse]) =
  Pretty.parens $ (prettyExpr 0 condition) <+> (Pretty.char '?') <+>
                    (prettyExpr 0 ifTrue) <+> Pretty.colon <+>
		    (prettyExpr 0 ifFalse)
prettyExpr _ (Application _ name arguments) =
  (Pretty.text name) <> Pretty.parens (prettyExprList arguments) <> Pretty.semi
prettyExpr _ (Exists _ vars term) =
  Pretty.text "exists" <+> (Pretty.parens (prettyParameters vars)) <>
    Pretty.colon <+> (prettyExpr 0 term)
prettyExpr _ (Forall _ vars term) =
  Pretty.text "forall" <+> (Pretty.parens (prettyParameters vars)) <>
    Pretty.colon <+> (prettyExpr 0 term)
prettyExpr _ (Comprehension _ parameter term) =
  Pretty.brackets $ prettyParameter parameter <+> Pretty.char '|' <+>
    prettyExpr 0 term
prettyExpr _ (EnumerationExpression _ exprs) =
  Pretty.brackets $ prettyExprList exprs
prettyExpr _ (RangeExpression _ min max) =
  prettyExpr 0 min <+> Pretty.text ".." <+> prettyExpr 0 max
prettyExpr _ (SpawnExpression _ name arguments) =
  Pretty.text "spawn" <+> Pretty.text name <>
    Pretty.parens (prettyExprList arguments)
prettyExpr _ (Otherwise _) =
  Pretty.text "otherwise"

unaryOps :: Map.Map String Int
unaryOps = Map.fromList [ ("not", 41),
                          ("-", 41) ]

binaryInfixOps :: Map.Map String Int
binaryInfixOps =
    Map.fromList [ ("iff", 11), ("implies", 11),
                   ("or", 12),
                   ("and", 13),
                   ("=", 14), ("/=", 14),
		   ("in", 15),
                   ("<", 16), ("<=", 16), (">", 16), (">=", 16),
                   ("-", 17), ("+", 17),
                   ("*", 18), ("/", 18), ("rem", 18), ("mod", 18) ]
\end{code}

Format a list of expressions.

\begin{code}
prettyExprList :: [Expression n] -> Doc
prettyExprList exprList = 
   Pretty.hsep $ Pretty.punctuate Pretty.comma (map (prettyExpr 0) exprList)
\end{code}



\subsection{Statements}
\label{sec:conch:ast:statements}

The data type \lstinline|Statement m n| specifies the abstract syntax
of statements. The type has two parameters for two kinds of
annotations. The first parameter \lstinline|m| is the type of
annotations that apply to the statements itself, whereas the second
parameter is the type of annotations that apply to expressions that
occur in statements. The reason for two parameter is: Statements carry
other annotations that expressions. For example, expressions may be
typed while statements are usually not.

Every statement has an optional string argument that represents a
possible label to that statement.

The intended meaning of the statements below are:
\begin{itemize}
\item \lstinline|Skip| represents a do-nothing statement.
\item \lstinline|AbstractStatement| is a statement that performs some
  computation without changing the visible state. Its purpose is to
  provide control points with meaningful names.
\item \lstinline|Await| waits until the argument expression evaluates to
  true and otherwise blocks execution.
\item \lstinline|Assignment| represents a possibly multiple assignment.
\item \lstinline|SpawnStatement| represents the creation of a process,
  just like \lstinline|SpawnExpression|, with the difference that the
  process identifier is ignored.
\item \lstinline|ProcedureCall| represents the call to a procedure or
  special operations. This includes the semaphore operations \texttt{P}
  and \texttt{V} and the monitor operations \texttt{wait}, \texttt{signal},
  and \texttt{signal\textunderscore all}.
\item \lstinline|MethodCall| represents the call to a procedure in a
  monitor or the method of an object.
\item \lstinline|If| represents a non-deterministic choice over conditions.
  A branch is represented by an \lstinline|Expression| and a sequence of
  \lstinline|Statements|. The idea is to execute the sequence of statements
  if the expression evaluates to true. If more than one branch evaluates to
  true, then one branch is nondeterministically chosen. The special
  expression \lstinline|Otherwise| is the negation of the disjunction of all
  other branches. If no condition is true, then the statement does nothing.
\item \lstinline|Do| represents a non-deterministic loop.
  A branch is represented by an \lstinline|Expression| and a sequence of
  \lstinline|Statements|. The idea is to execute the sequence of statements
  if the expression evaluates to true. If more than one branch evaluates to
  true, then one branch is nondeterministically chosen. The special
  expression \lstinline|Otherwise| is the negation of the disjunction of all
  other branches. If no condition is true, then the loop terminates. As long
  as one guard is true, the loop keeps executing.
\item \lstinline|Co| represents the concurrent execution of its branches.
\item \lstinline|CoRange| represents the execution of many identical
  processes. A variable is declared that is available in the scope of
  the body of the process to create. The declaration is followed by an
  expression that should be of some collection type. For each member of
   the collection, one process is created and the value of the collection
   member is assigned to the variable.
\item \lstinline|Block| represents a parenthised statement. Statements can
  be placed in parenthesis to assign that group of statements a label.
\item \lstinline|Atomic| represents a group of statements that is to be
  executed atomically.
\item \lstinline|Assertion| is used for verification purposes. When
  verifying, the assertion serves as a specification or a proof obligation.
\end{itemize}

\begin{code}
data Statement m n =
    Skip m (Maybe String)
  | AbstractStatement m (Maybe String) String
  | Await m (Maybe String) (Expression n)
  | Assignment m (Maybe String) [(Expression n, Expression n)]
  | SpawnStatement m (Maybe String) String [Expression n]
  | ProcedureCall m (Maybe String) String [Expression n]
  | MethodCall m (Maybe String) (Expression n) String [Expression n]
  | If m (Maybe String) [(Expression n, [Statement m n])]
  | Loop m (Maybe String) (Maybe (Expression n))
      [(Expression n, [Statement m n])]
  | Co m (Maybe String) [[Statement m n]]
  | CoRange m (Maybe String) (Parameter n) (Expression n) [Statement m n]
  | Block m (Maybe String) [Statement m n]
  | Atomic m (Maybe String) [Statement m n]
  | Assertion m (Maybe String) (Expression n)
  deriving (Eq)

instance Show (Statement m n) where
  show stmt = Pretty.render $ prettyStatement stmt
\end{code}

\begin{code}
stmtMap :: (m -> l) -> Statement m n -> Statement l n
stmtMap f (Skip m label) = Skip (f m) label
stmtMap f (AbstractStatement m label text) = AbstractStatement (f m) label text
stmtMap f (Await m label condition) = Await (f m) label condition
stmtMap f (Assignment m label assignments) = Assignment (f m) label assignments
stmtMap f (SpawnStatement m label process arguments) =
  SpawnStatement (f m) label process arguments
stmtMap f (ProcedureCall m label procedure arguments) =
  ProcedureCall (f m) label procedure arguments
stmtMap f (MethodCall m label receiver method arguments) =
  MethodCall (f m) label receiver method arguments
stmtMap f (If m label blocks) =
  If (f m) label (fmap (\(condition, body) -> (condition, fmap (stmtMap f) body)) blocks)
stmtMap f (Loop m label inv blocks) =
  Loop (f m) label inv (fmap (\(condition, body) -> (condition, fmap (stmtMap f) body)) blocks)
stmtMap f (Co m label forks) =
  Co (f m) label (fmap (fmap (stmtMap f)) forks)
stmtMap f (CoRange m label id range fork) =
  CoRange (f m) label id range (fmap (stmtMap f) fork)
stmtMap f (Block m label block) =
  Block (f m) label (fmap (stmtMap f) block)
stmtMap f (Atomic m label block) =
  Atomic (f m) label (fmap (stmtMap f) block)
stmtMap f (Assertion m label assertion) =
  Assertion (f m) label assertion
\end{code}

\begin{code}
exprMap :: (n -> l) -> Statement m n -> Statement m l
exprMap _ (Skip m label) = Skip m label
exprMap _ (AbstractStatement m label text) = AbstractStatement m label text
exprMap f (Await m label condition) = Await m label (fmap f condition)
exprMap f (Assignment m label assignments) =
  Assignment m label (fmap (\(l, r) -> (fmap f l, fmap f r)) assignments)
exprMap f (SpawnStatement m label process arguments) =
  SpawnStatement m label process (fmap (fmap f) arguments)
exprMap f (ProcedureCall m label procedure arguments) =
  ProcedureCall m label procedure (fmap (fmap f) arguments)
exprMap f (MethodCall m label receiver method arguments) =
  MethodCall m label (fmap f receiver) method (fmap (fmap f) arguments)
exprMap f (If m label blocks) =
  If m label (fmap (\(condition, body) -> (fmap f condition, fmap (exprMap f) body)) blocks)
exprMap f (Loop m label inv blocks) =
  Loop m label (fmap (fmap f) inv) (fmap (\(condition, body) -> (fmap f condition, fmap (exprMap f) body)) blocks)
exprMap f (Co m label forks) =
  Co m label (fmap (fmap (exprMap f)) forks)
exprMap f (CoRange m label id range fork) =
  CoRange m label (fmap f id) (fmap f range) (fmap (exprMap f) fork)
exprMap f (Block m label block) =
  Block m label (fmap (exprMap f) block)
exprMap f (Atomic m label block) =
  Atomic m label (fmap (exprMap f) block)
exprMap f (Assertion m label assertion) =
  Assertion m label (fmap f assertion)
\end{code}

\begin{code}
setStatementLabel :: String -> Statement m n -> Statement m n
setStatementLabel label (Skip m _) = Skip m (Just label)
setStatementLabel label (AbstractStatement m _ string) =
  AbstractStatement m (Just label) string
setStatementLabel label (Await m _ cond) = Await m (Just label) cond
setStatementLabel label (Assignment m _ list) = Assignment m (Just label) list
setStatementLabel label (SpawnStatement m _ process arguments) =
  SpawnStatement m (Just label) process arguments
setStatementLabel label (ProcedureCall m _ procedure arguments) =
  ProcedureCall m (Just label) procedure arguments
setStatementLabel label (MethodCall m _ receiver method arguments) =
  MethodCall m (Just label) receiver method arguments
setStatementLabel label (If m _ cases) = If m (Just label) cases
setStatementLabel label (Loop m _ inv cases) = Loop m (Just label) inv cases
setStatementLabel label (Co m _ forks) = Co m (Just label) forks
setStatementLabel label (CoRange m _ id range fork) =
  CoRange m (Just label) id range fork
setStatementLabel label (Block m _ block) = Block m (Just label) block
setStatementLabel label (Atomic m _ block) = Atomic m (Just label) block
setStatementLabel label (Assertion m _ assertion) = Assertion m (Just label) assertion
\end{code}

\begin{code}
prettyStatement :: Statement a b -> Doc
prettyStatement (Skip _ label) =
  prettyLabel label <+> Pretty.text "skip" <> Pretty.semi
prettyStatement (AbstractStatement _ label description) =
  prettyLabel label <+> Pretty.text description <> Pretty.semi
prettyStatement (Await _ label condition) =
  prettyLabel label <+> Pretty.text "await" <+> prettyExpr 0 condition <>
    Pretty.semi
prettyStatement (Assignment _ label assignments) =
  prettyLabel label <+> slhs <+> (Pretty.text ":=") <+> srhs <> Pretty.semi
  where
   (lhs, rhs) = unzip assignments
   slhs = prettyExprList lhs
   srhs = prettyExprList rhs
prettyStatement (SpawnStatement _ label process arguments) =
  prettyLabel label <+> Pretty.text "spawn" <+> Pretty.text process <>
    Pretty.parens (prettyExprList arguments) <> Pretty.semi
prettyStatement (ProcedureCall _ label procedure arguments) =
  prettyLabel label <+> Pretty.text procedure <>
    Pretty.parens (prettyExprList arguments) <> Pretty.semi
prettyStatement (MethodCall _ label receiver method arguments) =
  prettyLabel label <+> prettyExpr 0 receiver <> Pretty.char '.' <>
    Pretty.text method <> Pretty.parens (prettyExprList arguments) <>
    Pretty.semi
prettyStatement (If _ label cases) =
  prettyLabel label <+>
  (Pretty.text "if") $+$
  Pretty.vcat (map printClause cases)
  $+$ (Pretty.text "fi") <> Pretty.semi
prettyStatement (Loop _ label Nothing cases) =
  prettyLabel label <+>
    (Pretty.text "do") $+$ Pretty.vcat (map printClause cases)
    $+$ (Pretty.text "od") <> Pretty.semi
prettyStatement (Loop _ label (Just inv) cases) =
  prettyLabel label <+> Pretty.braces (prettyExpr 0 inv) <+>
    (Pretty.text "do") <+> Pretty.vcat (map printClause cases)
    $+$ (Pretty.text "od") <> Pretty.semi
prettyStatement (Co _ label forks) =
  prettyLabel label <+> Pretty.text "co" $+$
    Pretty.vcat (map (\s -> Pretty.text "||" <+> Pretty.nest 4 (prettyStatements s)) forks)
    $+$ Pretty.text "oc" <> Pretty.semi
prettyStatement (CoRange _ label id range fork) =
  prettyLabel label <+> Pretty.text "co" <+>
   Pretty.brackets (prettyParameter id <+> Pretty.text "in" <+> prettyExpr 0 range) $+$
    prettyStatements fork
    $+$ Pretty.text "oc" <> Pretty.semi
prettyStatement (Block _ label stmts) =
  prettyLabel label <+>
    Pretty.text "[|" <+> Pretty.nest 4 (prettyStatements stmts)
      <+> Pretty.text "|]" <> Pretty.semi
prettyStatement (Atomic _ label stmts) =
  prettyLabel label <+>
    Pretty.text "<|" <+> Pretty.nest 4 (prettyStatements stmts)
      <+> Pretty.text "|>" <> Pretty.semi
prettyStatement (Assertion _ label expr) =
  prettyLabel label <+> Pretty.braces (prettyExpr 0 expr)
\end{code}



\begin{code}
printClause :: (Expression n, [Statement m n]) -> Pretty.Doc
printClause (LiteralTrue _, stmts) =
    Pretty.text "[]" <+> (Pretty.nest 5 (prettyStatements stmts))
printClause (Otherwise _, stmts) =
    Pretty.text "[]" <+> Pretty.text "otherwise" <+>
      (Pretty.nest 5 (prettyStatements stmts))
printClause (cond, stmts) =
  Pretty.text "[]" <+> (prettyExpr 0 cond) <+> (Pretty.text "->") $+$
    (Pretty.nest 5 (prettyStatements stmts))
\end{code}



\begin{code}
prettyStatements :: [Statement m n] -> Pretty.Doc
prettyStatements stmts = Pretty.vcat (map prettyStatement stmts)
\end{code}


\begin{code}
prettyLabel :: (Maybe String) -> Pretty.Doc
prettyLabel (Just label) = Pretty.text label <> Pretty.char ':'
prettyLabel Nothing = Pretty.empty
\end{code}


\subsection{Formal Parameter Declaration}
\label{sec:conch:ast:parameter}

\begin{code}
data Parameter n =
    Parameter String (Type n)
  deriving (Eq)

instance Functor Parameter where
  fmap f (Parameter name typ) = Parameter name (fmap f typ)

parameterName :: Parameter n -> String
parameterName (Parameter name _) = name

parameterType :: Parameter n -> Type n
parameterType (Parameter _ typ) = typ

instance Show (Parameter n) where
  show parameter = Pretty.render $ prettyParameter parameter

prettyParameter :: Parameter n -> Pretty.Doc
prettyParameter (Parameter name typ) =
  Pretty.text name <> Pretty.text ":" <+> prettyType typ

prettyParameters :: [Parameter n] -> Pretty.Doc
prettyParameters parameters =
  Pretty.hsep (Pretty.punctuate Pretty.comma (map prettyParameter (parameters)))
\end{code}

Often, we just need the set of name occurring in a list of parameters.

\begin{code}
parameterNames :: [Parameter n] -> Set String
parameterNames parameters = Set.fromList $ map parameterName parameters
\end{code}



\subsection{Variable Declaration}
\label{sec:conch:ast:parameter}

\begin{code}
data Variable n = Variable String (Type n) (Maybe (Expression n))

variableName :: Variable n -> String
variableName (Variable name _ _) = name

variableType :: Variable n -> Type n
variableType (Variable _ typ _) = typ

variableInit :: Variable n -> Maybe (Expression n)
variableInit (Variable _ _ init) = init

instance Show (Variable n) where
  show variable = Pretty.render $ prettyVariable variable

prettyVariable :: Variable n -> Pretty.Doc
prettyVariable (Variable name typ Nothing) =
  Pretty.text "variable" <+>
  Pretty.text name <> Pretty.text ":" <+> prettyType typ
prettyVariable (Variable name typ (Just init)) =
  prettyVariable (Variable name typ Nothing) <+> Pretty.text ":="
    <+> prettyExpr 0 init
\end{code}



\subsection{Process Type Definition}
\label{sec:conch:ast:process}

A \emph{process type} defines a template for processes.

\begin{code}
data Process m n =
  Process { processName :: String,
            processParameters :: [Parameter n],
            processBody :: [Statement m n] }

instance Show (Process m n) where
  show process = Pretty.render $ prettyProcess process

prettyProcess :: Process m n -> Pretty.Doc
prettyProcess process =
  Pretty.text "process" <+> Pretty.text (processName process) <+>
    Pretty.lparen <> prettyParameters (processParameters process) <>
    Pretty.rparen $+$
    Pretty.nest 4 (prettyStatements (processBody process)) $+$
    Pretty.text "end"
\end{code}



\subsection{Procedure declaration}
\label{sec:conch:ast:procedure}

\begin{code}
data Procedure m n =
  Procedure String [Parameter n] [Statement m n]
    (Maybe (Expression n, Expression n))
  deriving (Eq)

instance Show (Procedure m n) where
  show procedure = Pretty.render $ prettyProcedure procedure
\end{code}

\begin{code}
prettyProcedure :: Procedure m n -> Pretty.Doc
prettyProcedure (Procedure name parameters body Nothing) =
  Pretty.text "procedure" <+> Pretty.text name <>
    Pretty.parens (prettyParameters parameters) $+$
    (Pretty.nest 4 (prettyStatements body)) $+$ Pretty.text "end"
prettyProcedure (Procedure name parameters body (Just (pre, post))) =
  Pretty.text "procedure" <+> Pretty.text name <>
    Pretty.parens (prettyParameters parameters) $+$
    (Pretty.nest 4 (prettyStatements body)) $+$ Pretty.text "end"
\end{code}


\subsection{Declarations}
\label{sec:conch:ast:unit}

\begin{code}
data Declaration m n =
    ProcedureDeclaration (Procedure m n)
  | ProcessDeclaration (Process m n)
  | VariableDeclaration (Variable n)
\end{code}

\begin{code}
instance Show (Declaration m n) where
  show declaration = Pretty.render $ prettyDeclaration declaration
\end{code}

\begin{code}
prettyDeclaration :: Declaration m n -> Pretty.Doc
prettyDeclaration (ProcessDeclaration process) = prettyProcess process
prettyDeclaration (ProcedureDeclaration procedure) = prettyProcedure procedure
prettyDeclaration (VariableDeclaration variable) = prettyVariable variable
\end{code}



\subsection{Translation Unit}
\label{sec:conch:ast:unit}

A unit of the abstract syntax tree contains all possible declarations.
The declarations are maintained in dictionaries for easier access.

\begin{code}
data Unit m n =
  Unit { unitDeclarations :: [Declaration m n],
         unitInit :: [Statement m n] }

instance Show (Unit m n) where
  show unit = Pretty.render $ prettyUnit unit

prettyUnit :: Unit m n -> Pretty.Doc
prettyUnit unit =
  Pretty.vcat (map prettyDeclaration (unitDeclarations unit)) $+$
  (prettyStatements (unitInit unit))
\end{code}
