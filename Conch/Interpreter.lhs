%%%
%%%  conch -- Animate, simulate, and analyse concurrent programs.
%%%  Copyright (C) 2012 Marcel Kyas
%%%
%%%  This program is free software: you can redistribute it and/or modify
%%%  it under the terms of the GNU General Public License as published by
%%%  the Free Software Foundation, either version 3 of the License, or
%%%  (at your option) any later version.
%%%
%%%  This program is distributed in the hope that it will be useful,
%%%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%%%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%%  GNU General Public License for more details.
%%%
%%%  You should have received a copy of the GNU General Public License
%%%  along with this program.  If not, see <http://www.gnu.org/licenses/>.

\section{Interpreter}
\label{sec:conch:interpreter}

The module \lstinline|Conch.Interpreter| implements a very simple
interpreter.

\begin{code}
{-# OPTIONS_GHC -W #-}
module Conch.Interpreter where

import Data.Map (Map)
import qualified Data.Map as Map
import Conch.AST
\end{code}


\subsection{Values}
\label{sec:conch:interpreter:values}

\begin{code}
data Value =
    Bool Bool
  | Integer Integer
  | List [Value]
\end{code}



\subsection{State}
\label{sec:conch:interpreter:state}

\begin{code}
type State = Map String Value
\end{code}



\subsection{Evaluate Expression}
\label{sec:conch:interpreter:evaluate}

\begin{code}
evaluate :: State -> Expression n -> Value
evaluate _ (LiteralTrue _) = Bool True
evaluate _ (LiteralFalse _) = Bool False
evaluate _ (LiteralInteger _ value) = Integer value
evaluate state (Identifier _ name) = state Map.! name
evaluate state (Subscript _ array what) =
  arrayValue !! (fromIntegral whatValue)
  where List arrayValue = evaluate state array
        Integer whatValue = evaluate state what
\end{code}
