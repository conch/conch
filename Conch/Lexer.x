-- -*- mode:haskell -*-
--
--  conch -- Animate, simulate, and analyse concurrent programs.
--  Copyright (C) 2012 Marcel Kyas
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

{
module Conch.Lexer where
}

%wrapper "posn"

@id         = [A-Za-z][A-Za-z'_]*
@number     = [0-9]+
@abstract   = \`[^\`\n]*\`

tokens :-
  $white+ ;
  "--".* ;
  "."       { \p s -> TokenDot p }
  ".."      { \p s -> TokenDotDot p }
  ","       { \p s -> TokenComma p }
  ":="      { \p s -> TokenAssign p }
  ":"       { \p s -> TokenColon p }
  ";"       { \p s -> TokenSemicolon p }
  "[]"      { \p s -> TokenBox p }
  "->"      { \p s -> TokenLeftArrow p }
  "-"       { \p s -> TokenMinus p }
  "+"       { \p s -> TokenPlus p }
  "*"       { \p s -> TokenStar p }
  "/"       { \p s -> TokenSlash p }
  "^"       { \p s -> TokenHat p }
  "<"       { \p s -> TokenLt p }
  "<="      { \p s -> TokenLtEq p }
  ">"       { \p s -> TokenGt p }
  ">="      { \p s -> TokenGtEq p }
  "="       { \p s -> TokenEq p }
  "/="      { \p s -> TokenNEq p }
  "["       { \p s -> TokenLeftBracket p }
  "]"       { \p s -> TokenRightBracket p }
  "{"       { \p s -> TokenLeftBrace p }
  "|"       { \p s -> TokenBar p }
  "||"      { \p s -> TokenBarBar p }
  "}"       { \p s -> TokenRightBrace p }
  "("       { \p s -> TokenLeftParenthesis p }
  ")"       { \p s -> TokenRightParenthesis p }
  "[|"      { \p s -> TokenStartBlock p }
  "|]"      { \p s -> TokenEndBlock p }
  "<|"      { \p s -> TokenStartAtomic p }
  "|>"      { \p s -> TokenEndAtomic p }
  and       { \p s -> TokenAnd p }
  await     { \p s -> TokenAwait p }
  co        { \p s -> TokenCo p }
  do        { \p s -> TokenDo p }
  else      { \p s -> TokenElse p }
  end       { \p s -> TokenEnd p }
  ensure    { \p s -> TokenEnsure p }
  exists    { \p s -> TokenExists p }
  fi        { \p s -> TokenFi p }
  forall    { \p s -> TokenForall p }
  if        { \p s -> TokenIf p }
  in        { \p s -> TokenIn p }
  mod       { \p s -> TokenMod p }
  not       { \p s -> TokenNot p }
  oc        { \p s -> TokenOc p }
  od        { \p s -> TokenOd p }
  or        { \p s -> TokenOr p }
  otherwise { \p s -> TokenOtherwise p }
  process   { \p s -> TokenProcess p }
  procedure { \p s -> TokenProcedure p }
  rem       { \p s -> TokenRem p }
  require   { \p s -> TokenRequire p }
  skip      { \p s -> TokenSkip p }
  spawn     { \p s -> TokenSpawn p }
  then      { \p s -> TokenThen p }
  variable  { \p s -> TokenVariable p }
  @id       { \p s -> TokenIdentifier p s }
  @number   { \p s -> TokenInteger p (read s) }
  @abstract { \p s -> TokenAbstract p s }
  

{
data Token =
       TokenProcess AlexPosn
     | TokenProcedure AlexPosn
     | TokenEnsure AlexPosn
     | TokenRequire AlexPosn
     | TokenVariable AlexPosn
     | TokenAwait AlexPosn
     | TokenInit AlexPosn
     | TokenEnd AlexPosn
     | TokenIf AlexPosn
     | TokenIn AlexPosn
     | TokenFi AlexPosn
     | TokenThen AlexPosn
     | TokenElse AlexPosn
     | TokenDo AlexPosn
     | TokenOd AlexPosn
     | TokenCo AlexPosn
     | TokenOc AlexPosn
     | TokenSkip AlexPosn
     | TokenSpawn AlexPosn
     | TokenOtherwise AlexPosn
     | TokenAnd AlexPosn
     | TokenOr AlexPosn
     | TokenImplies AlexPosn
     | TokenIff AlexPosn
     | TokenMod AlexPosn
     | TokenRem AlexPosn
     | TokenExists AlexPosn
     | TokenForall AlexPosn
     | TokenFalse AlexPosn
     | TokenTrue AlexPosn
     | TokenLt AlexPosn
     | TokenLtEq AlexPosn
     | TokenGt AlexPosn
     | TokenGtEq AlexPosn
     | TokenEq AlexPosn
     | TokenNEq AlexPosn
     | TokenDot AlexPosn
     | TokenDotDot AlexPosn
     | TokenComma AlexPosn
     | TokenSemicolon AlexPosn
     | TokenColon AlexPosn
     | TokenAssign AlexPosn
     | TokenBox AlexPosn
     | TokenLeftArrow AlexPosn
     | TokenNot AlexPosn
     | TokenMinus AlexPosn
     | TokenPlus AlexPosn
     | TokenStar AlexPosn
     | TokenSlash AlexPosn
     | TokenBar AlexPosn
     | TokenBarBar AlexPosn
     | TokenHat AlexPosn
     | TokenLeftBracket AlexPosn
     | TokenRightBracket AlexPosn
     | TokenLeftBrace AlexPosn
     | TokenRightBrace AlexPosn
     | TokenLeftParenthesis AlexPosn
     | TokenRightParenthesis AlexPosn
     | TokenStartAtomic AlexPosn
     | TokenEndAtomic AlexPosn
     | TokenStartBlock AlexPosn
     | TokenEndBlock AlexPosn
     | TokenIdentifier AlexPosn String
     | TokenAbstract AlexPosn String
     | TokenString AlexPosn String
     | TokenInteger AlexPosn Integer
     deriving (Eq, Show)
}
