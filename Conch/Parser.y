-- -*- mode: haskell -*-
--
--  conch -- Animate, simulate, and analyse concurrent programs.
--  Copyright (C) 2012 Marcel Kyas
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

{
module Conch.Parser where
import System.IO
import Conch.Lexer
import Conch.AST
}
%name parseConch
%tokentype { Token }
%error { parseError }

%token
    and                 { TokenAnd posn }
    await               { TokenAwait posn }
    co                  { TokenCo posn }
    do                  { TokenDo posn }
    else                { TokenElse posn }
    end                 { TokenEnd posn }
    ensure              { TokenEnsure posn }
    exists              { TokenExists posn }
    false               { TokenFalse posn }
    fi                  { TokenFi posn }
    forall              { TokenForall posn }
    if                  { TokenIf posn }
    iff                 { TokenIff posn }
    implies             { TokenImplies posn }
    in                  { TokenIn posn }
    mod                 { TokenMod posn }
    not                 { TokenNot posn }
    oc                  { TokenOc posn }
    od                  { TokenOd posn }
    or                  { TokenOr posn }
    otherwise           { TokenOtherwise posn }
    procedure           { TokenProcedure posn }
    process             { TokenProcess posn }
    rem                 { TokenRem posn }
    require             { TokenRequire posn }
    skip                { TokenSkip posn }
    spawn               { TokenSpawn posn }
    then                { TokenThen posn }
    true                { TokenTrue posn }
    variable            { TokenVariable posn }
    '+'                 { TokenPlus posn }
    '-'                 { TokenMinus posn }
    '*'                 { TokenStar posn }
    '/'                 { TokenSlash posn }
    '<'                 { TokenLt posn }
    '<='                { TokenLtEq posn }
    '>'                 { TokenGt posn }
    '>='                { TokenGtEq posn }
    '='                 { TokenEq posn }
    '/='                { TokenNEq posn }
    '.'                 { TokenDot posn }
    '..'                { TokenDotDot posn }
    ','                 { TokenComma posn }
    ';'                 { TokenSemicolon posn }
    ':'                 { TokenColon posn }
    ':='                { TokenAssign posn }
    '->'                { TokenLeftArrow posn }
    '[]'                { TokenBox posn }
    '['                 { TokenLeftBracket posn }
    ']'                 { TokenRightBracket posn }
    '[|'                { TokenStartBlock posn }
    '|]'                { TokenEndBlock posn }
    '<|'                { TokenStartAtomic posn }
    '|>'                { TokenEndAtomic posn }
    '{'                 { TokenLeftBrace posn }
    '|'                 { TokenBar posn }
    '||'                { TokenBarBar posn }
    '}'                 { TokenRightBrace posn }
    '('                 { TokenLeftParenthesis posn }
    ')'                 { TokenRightParenthesis posn }
    id                  { TokenIdentifier posn $$ }
    number              { TokenInteger posn $$ }
    abstract            { TokenAbstract posn $$ }


%left implies iff
%left or
%left and
%right in
%left '+' '-'
%left '*' '/' mod rem
%left NEG

%%

Unit :
    {- empty -}                 { Unit { unitDeclarations = [], unitInit = [] } }
  | Declarations                { Unit { unitDeclarations = reverse $1, unitInit = [] } }
  | Init                        { Unit { unitDeclarations = [], unitInit = reverse $1 } }
  | Declarations Init           { Unit { unitDeclarations = reverse $1, unitInit = reverse $2 } }

Declarations :
    Declaration                         { [$1] }
  | Declarations ';'                    { $1 }
  | Declarations Declaration            { $2 : $1 }

Declaration :
    Variable                            { Conch.AST.VariableDeclaration $1 }
  | Process                             { Conch.AST.ProcessDeclaration $1 }
  | Procedure                           { Conch.AST.ProcedureDeclaration $1 }

Init :
    Statements                          { reverse $1 }

Variable :
    variable Parameter                  { parameterToVariable $2 Nothing }
  | variable Parameter ':=' Expr        { parameterToVariable $2 (Just $4) }

Variables : 
    Variable                            { [$1] }
  | Variables ';' Variable              { $3 : $1 }

Process :
    process id '(' ')' Body end
      { let (_, body) = $5 in Process{ processName = $2, processParameters = [], processBody = body } }
  | process id '(' Parameters ')' Body end
      { let (_, body) = $6 in Process{ processName = $2, processParameters = reverse $4, processBody = body } }

Procedure :
    procedure id '(' ')' Body end
        { let (_, body) = $5 in Procedure $2 [] body Nothing }
  | procedure id '(' Parameters ')' Body end
        { let (_, body) = $6 in Procedure $2 (reverse $4) body Nothing }

Body :
    {- Empty -}                         { ([], []) }
  | Statements                          { ([], reverse $1) }
  | Variables ';' Statements            { (reverse $1, reverse $3) }
  | Variables                           { (reverse $1, []) }

Statements :
    Statements ';' LabelledStatement    { $3 : $1 }
  | Statements ';'                      { $1 }
  | LabelledStatement                   { [$1] }

LabelledStatement :
    UnlabelledStatement             { $1 }
  | id ':' UnlabelledStatement      { Conch.AST.setStatementLabel $1 $3 }

UnlabelledStatement :
    Assertion                 { Conch.AST.Assertion () Nothing $1 }
  | UnlabelledStmtNoAssert    { $1 }

UnlabelledStmtNoAssert:
    skip                      { Conch.AST.Skip () Nothing }
  | abstract                  { Conch.AST.AbstractStatement () Nothing $1 }
  | LhsList ':=' ExprList     { Conch.AST.Assignment () Nothing (zip $1 $3) }
  | await Expr                { Conch.AST.Await () Nothing $2 }
  | spawn id '(' ')'          { Conch.AST.SpawnStatement () Nothing $2 [] }
  | spawn id '(' ExprList ')' { Conch.AST.SpawnStatement () Nothing $2 (reverse $4) }
  | if CaseList fi            { Conch.AST.If () Nothing (reverse $2) }
  | if '[]' CaseList fi       { Conch.AST.If () Nothing (reverse $3) }
  | do '[]' CaseList od       { Conch.AST.Loop () Nothing Nothing (reverse $3) }
  | do Assertion '[]' CaseList od  { Conch.AST.Loop () Nothing (Just $2) (reverse $4) }
  | co ForkList oc            { Conch.AST.Co () Nothing (reverse $2) }
  | co '||' ForkList oc       { Conch.AST.Co () Nothing (reverse $3) }
  | co '[' Parameter in Expr ']' Statements oc { Conch.AST.CoRange () Nothing $3 $5 (reverse $7) }
  | id '(' ')'                { Conch.AST.ProcedureCall () Nothing $1 [] }
  | id '(' ExprList ')'       { Conch.AST.ProcedureCall () Nothing $1 (reverse $3) }
  | SelectExpr '(' ')'        { let Select _ callee method = $1 in Conch.AST.MethodCall () Nothing callee method [] }
  | SelectExpr '(' ExprList ')'  { let Select _ callee method = $1 in Conch.AST.MethodCall () Nothing callee method (reverse $3) }
  | '[|' Statements '|]'      { Conch.AST.Block () Nothing $2 }
  | '<|' UnlabelledStmtNoAssert '|>' { Conch.AST.Atomic () Nothing [$2] }
  | '<|' UnlabelledStmtNoAssert ';' '|>' { Conch.AST.Atomic () Nothing [$2] }
  | '<|' UnlabelledStmtNoAssert ';' Statements '|>' { Conch.AST.Atomic () Nothing ($2:(reverse $4)) }

CaseList :
    Cases                               { $1 }
  | Cases '[]' Otherwise                { $3 : $1 }

Cases :
    Case                                { [$1] }
  | Cases '[]' Case                     { $3 : $1 } 

Case :
    Expr '->' Statements               { ($1, reverse $3) }
  | Statements                         { (LiteralTrue (), reverse $1) }

Otherwise :
    otherwise Statements              { (Conch.AST.Otherwise (), reverse $2) }
  | otherwise '->' Statements         { (Conch.AST.Otherwise (), reverse $3) }

ForkList :
    Statements                         { [$1] }
  | ForkList '||' Statements           { $3 : $1 }

Assertion :
    '{' Expr '}'                        { $2 }

LhsList :
    Lhs                                 { [$1] }
  | LhsList ',' Lhs                     { $3 : $1 }

Lhs:
    PostfixExpr                         { $1 }


---
-- Expressions
---

ExprList :
    Expr                                { [$1] }
  | ExprList ',' Expr                   { $3 : $1 }

Expr :
    ConditionalExpr                      { $1 }

ConditionalExpr :
    QuantifiedExpr                      { $1 }
  | if ConditionalExpr then ConditionalExpr else ConditionalExpr fi  { ConditionalExpression () $2 $4 $6 }

QuantifiedExpr :
    LogicalExpr                         { $1 }
  | forall '(' Parameters ')' ':' QuantifiedExpr  { Forall () (reverse $3) $6 }
  | exists '(' Parameters ')' ':' QuantifiedExpr  { Exists () (reverse $3) $6 }

LogicalExpr :
    RelationalExpr                      { $1 }
  | LogicalExpr LogicalOp RelationalExpr  { Application () $2 [$1, $3] }

LogicalOp :
    and                                 { "and" }
  | or                                  { "or" }
  | implies                             { "implies" }
  | iff                                 { "iff" }

RelationalExpr :
    ArithmeticExpr                      { $1 }
  | ArithmeticExpr RelationalOp ArithmeticExpr  { Application () $2 [$1, $3] }

RelationalOp :
    '<'                                 { "<" }
  | '<='                                { "<=" }
  | '>='                                { ">=" }
  | '>'                                 { ">" }
  | '='                                 { "=" }
  | '/='                                { "/=" }
  | in                                  { "in" }

RangeExprList :
    RangeExpr                           { [$1] }
  | RangeExprList ',' RangeExpr         { $3 : $1 }

RangeExpr :
    ArithmeticExpr                      { $1 }
  | ArithmeticExpr '..' ArithmeticExpr  { RangeExpression () $1 $3 }

ArithmeticExpr :
    AtomicExpr                          { $1 }
  | ArithmeticExpr BinaryOp AtomicExpr  { Application () $2 [$1, $3] }

BinaryOp :
    '+'                                 { "+" }
  | '-'                                 { "-" }
  | '*'                                 { "*" }
  | '/'                                 { "/" }
  | mod                                 { "mod" }
  | rem                                 { "rem" }

AtomicExpr :
    ApplicationExpr                     { $1 }
  | PostfixExpr                         { $1 }
  | LiteralExpr                         { $1 }
  | '-' AtomicExpr %prec NEG            { Application () "-" [$2] }
  | not AtomicExpr                      { Application () "not" [$2] }
  | '(' Expr ')'                        { $2 }
  | '{' Parameter '|' Expr '}'          { Comprehension () $2 $4 }
  | '[' ']'                             { EnumerationExpression () [] }
  | '[' RangeExprList ']'               { EnumerationExpression () (reverse $2) }
  | spawn id '(' ')'                    { SpawnExpression () $2 [] }
  | spawn id '(' ExprList ')'           { SpawnExpression () $2 (reverse $4) }

ApplicationExpr :
    id '(' ')'                          { Application () $1 [] }
  | id '(' ExprList ')'                 { Application () $1 (reverse $3) }

PostfixExpr :
    id                                  { Identifier () $1 }
  | PostfixExpr '[' Expr ']'            { Subscript () $1 $3 }
  | SelectExpr                          { $1 }

SelectExpr :
    PostfixExpr '.' id                  { Select () $1 $3 }

LiteralExpr :
    true                                { LiteralTrue () }
  | false                               { LiteralFalse () }
  | number                              { LiteralInteger () $1 }


---
-- Declarations
---
Parameters :
    Parameter                   { [$1] }
  | Parameters ',' Parameter    { $3 : $1 }

Parameter :
    id ':' Type                 { Parameter $1 $3 }

---
-- Types
---
Type :
    id                          { Conch.AST.Simple () $1 }
  | Type '[' RangeExprList ']'  { Conch.AST.Array () $1 (reverse $3) }
  | '{' Enumerators '}'         { Conch.AST.Enumeration () (reverse $2) }
  | '{' Parameter '|' Expr '}'  { Conch.AST.PredicateType () $2 $4 }

Enumerators :
    id                                  { [$1] }
  | Enumerators ',' id                  { $3 : $1 }

{
-- Convert a parameter to a variable declaration
parameterToVariable :: Parameter n -> Maybe (Expression n) -> Variable n
parameterToVariable (Parameter name typ) initialiser =
  Variable name typ initialiser

parseError :: [Token] -> a
parseError [] = error "Parse error"
parseError (t:_) = error $ "Parse error at " ++ (show t)

parseFromFile :: String -> IO (Unit () ())
parseFromFile path =
  do input <- readFile path
     let tokens = alexScanTokens input
     -- putStrLn (show tokens)
     let tree = parseConch tokens
     return tree
}
